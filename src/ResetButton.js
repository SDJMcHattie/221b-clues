import React from 'react';

export default function ResetButton(props) {
  return (
    <div>
      <div className="d-flex flex-row-reverse">
        <div className="col-sm-3">
          <button
            type="button"
            className="btn btn-danger"
            data-toggle="modal"
            data-target="#confirmResetModal"
          >
            Reset All
          </button>
        </div>
      </div>

      <div className="modal fade" id="confirmResetModal" tabindex="-1" role="dialog" aria-labelledby="modelResetTitleLabel" aria-hidden="true">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-body">
              Are you sure you want to reset everything? All clues and notes will be lost.
            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-secondary" data-dismiss="modal">Cancel</button>
              <button
                type="button"
                className="btn btn-danger"
                data-dismiss="modal"
                onClick={() => props.resetState()}
              >
                Confirm Reset
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
