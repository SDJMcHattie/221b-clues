import React from 'react';

import FreeTextArea from './FreeTextArea.js';
import Location from './Location.js';
import ResetButton from './ResetButton.js';

import './App.css';
import BoxImage from './header.jpg';
import Clues from './clues.json';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = this.getStoredState() || this.initialState();
  }

  initialState() {
    const locations = [
      "Chemist",
      "Bank",
      "Carriage Depot",
      "Docks",
      "Hotel",
      "Locksmith",
      "Museum",
      "Newsagents",
      "Park",
      "Pawnbroker",
      "Theatre",
      "Boar's Head",
      "Scotland Yard",
      "Tobacconist",
    ];

    return {
      stateVersion: 1,
      caseScenarioText: '',
      locations: locations.map((locationName) => {
        return {
          displayName: locationName,
          clueCode: 0,
          clueText: '',
          revealClue: false,
        };
      }),
    };
  }

  setStateAndStore(state) {
    this.setState(
      state,
      () => {localStorage.setItem('AppState', JSON.stringify(this.state))});
  }

  getStoredState() {
    const storedStateString = localStorage.getItem('AppState');

    if (!storedStateString) {
      return null;
    }

    const storedState = JSON.parse(storedStateString);

    if (!storedState || !storedState.stateVersion || storedState.stateVersion < this.initialState().stateVersion) {
      return null;
    }

    return storedState;
  }

  resetState() {
    this.setStateAndStore(this.initialState());
  }

  updateScenarioText(newText) {
    this.setStateAndStore({
      caseScenarioText: newText,
    });
  }

  clueCodeChanged(index, newClueCode) {
    const locations = this.state.locations.slice();
    locations[index].clueCode = newClueCode;

    const newClue = Clues.find((clue) => clue.number === newClueCode);
    locations[index].clueText = newClue ? newClue.text : '';

    locations[index].revealClue = false;

    this.setStateAndStore({
      locations: locations,
    });
  }

  toggleClueVisible(index) {
    const locations = this.state.locations.slice();
    locations[index].revealClue = !locations[index].revealClue;

    this.setStateAndStore({
      locations: locations,
    });
  }

  render() {
    const locations = this.state.locations.map((location, index) =>
      <Location
        key={location.displayName}
        displayName={location.displayName}
        clueCode={location.clueCode}
        clueText={location.clueText}
        revealClue={location.revealClue}
        onClueCodeChanged={(newCode) => this.clueCodeChanged(index, newCode)}
        toggleClueVisible={() => this.toggleClueVisible(index)}
      />
    );

    return(
      <div className="container-narrow">
        <div className="jumbotron">
          <img src={BoxImage} title="221B box cover" alt="221B box cover" />
        </div>
        <ResetButton
          resetState={() => this.resetState()}
        />
        <div className="container">
          <FreeTextArea
            title="Case Scenario"
            text={this.state.caseScenarioText}
            onChange={(newText) => this.updateScenarioText(newText)}
          />
          {locations}
        </div>
        <div className="footer">
          <hr />
          <p>Created by Stuart McHattie</p>
        </div>
      </div>
    );
  }
}
