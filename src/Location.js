import React from 'react';
import './Location.css';

export default class Location extends React.Component {
  processClueCode(newCode) {
    if (newCode === '') {
      this.props.onClueCodeChanged(0);
      return;
    }

    const parsedCode = parseInt(newCode);
    if (!isNaN(parsedCode)) {
      this.props.onClueCodeChanged(parsedCode);
    }
  }

  render() {
    const clueCode = this.props.clueCode > 0
      ? this.props.clueCode
      : '';
    const clueCodeInvalid = this.props.clueText.length === 0;

    const buttonClass = this.props.revealClue ? 'btn-danger' : 'btn-primary';
    const buttonTitle = this.props.revealClue ? 'Hide' : 'Show';
    const cluePlaceholder = clueCodeInvalid
      ? 'Clue code is invalid'
      : 'Clue is hidden';

    return (
      <div>
        <hr />
        <div className="row">
          <div className="col-sm-3">
            <p className="location-name">{this.props.displayName}</p>
          </div>
          <div className="col-sm-2">
            <input
              className="location-code"
              placeholder="Code #"
              value={clueCode}
              onChange={(event) => this.processClueCode(event.target.value)}
            />
            <button
              disabled={clueCodeInvalid}
              type="button"
              className={"btn " + buttonClass}
              onClick={() => this.props.toggleClueVisible()}
            >
              {buttonTitle}
            </button>
          </div>
          <div className="col-sm-7">
            <textarea
              className="clue-box"
              readonly="true"
              placeholder={cluePlaceholder}
              value={this.props.revealClue ? this.props.clueText : ''}>
            </textarea>
          </div>
        </div>
      </div>
    );
  }
}
