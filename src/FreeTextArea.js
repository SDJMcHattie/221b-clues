import React from 'react';
import './FreeTextArea.css';

export default function FreeTextArea(props) {
  return (
    <div>
      <hr />
      <p className="free-text-title">{props.title}</p>
      <textarea
        className="free-text"
        onChange={(event) => props.onChange(event.target.value)}
        value={props.text}
      />
    </div>
  );
}
